// This file is a "Hello, world!" in C++ language by GCC for wandbox.
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <random>

int canary = 100;

namespace {    
    enum returndata : int {ok = 0, ko};
}

namespace named_func {
    void func() {
        // no nested func in c, only lambdas
        /*void f2(){}*/
        auto f = [](){
            std::cout << "__func__ " << __func__ << std::endl;
            std::cout << "__FUNCTION__ " << __FUNCTION__ << std::endl;
            std::cout << "__PRETTY_FUNCTION__ " << __PRETTY_FUNCTION__ << std::endl;
        };
        
        f();
    }
    
    returndata r = [](){
        std::cout << "nested_func with name" << std::endl;
        func();
        return ok;
    }();
}

namespace no_if {
    int func() {
        static std::uniform_real_distribution<double> distribution(0.0, 3.0);
        static std::mt19937 generator;
        return (int) distribution(generator);
    }

    void print1(){
        std::cout << "assurdo1" << std::endl;
    }

    void print2(){
        std::cout << "assurdo2" << std::endl;
    }

    void print3(){
        std::cout << "assurdo3" << std::endl;
    }
/*
    void test_not_working() {
        std::cout << "label" << (&&label) << std::endl;
        std::cout << "done" << (&&done) << std::endl;
        bool r = func();
        std::cout << "r" << r << std::endl;
        auto diff = &&done - &&label;
        std::cout << "diff" << f << std::endl;
        void * f = ((&&label) + (diff * r));
        std::cout << "f" << f << std::endl;
        ((void (*)()) f)();
        label:
        print1();
        print2();
        done:
        print3();
    }
   */ 

    void test() {
        auto r = func();
        std::cout << "r" << r << std::endl;
        // 0x5 goto print2(); 0xa goto print3();
        auto f = ((&&label) + (0x5 * r));
        std::cout << "f" << f << std::endl;
        std::cout << "label" << (&&label) << std::endl;
        ((void (*)()) f)();
        label:
        print1();
        print2();
        print3();
    }


    returndata r = [](){
        std::cout << "no_if" << r << std::endl;
        for(int i = 0; i < 10; i++) {
            test();
        }
        return ok;
    }();
}


namespace template_virtualizer {
     template<typename T>
     concept DoesEm = requires(T a) {
         { a.m() } -> std::same_as<int>;
     };

     template<typename T>
     concept DoesGi = requires(T a) {
         { a.g() } -> std::same_as<int>;
     };

     template<typename T>
     concept DoesEf = requires(T a) {
         { a.g() } -> std::same_as<int>;
     };

     template<typename T>
     concept DoesEmAndGi = DoesEm<T> && DoesGi<T>;
     
     struct s {
        int x = 2;
        s(int x) : x{x} {}
        virtual int m() const {return 0;};
        int g() const {return 3;};
    };
   
    struct z : s {
        using s::s;

        int m() const override {
            return x + this->g();
        }

        int f() const {
            return this->m() + this->g();
        }
    };
   
    auto r1 = [](DoesEm auto & o){
        return o.m();
    };
   
    auto r2 = [](DoesGi auto & o){
        return o.g();
    };
   
    auto r3 = [](DoesEf auto & o){
        return o.f();
    };
   
    auto r4 = [](DoesEmAndGi auto & o){
        std::cout << r1(o) << " " << r2(o) << std::endl;
    };
   
    returndata r = [](){
        s sz = z{1};
        z zz = z{2};
        s ss = s{3};
        std::vector<s *> x {&sz, &zz, &ss};
        std::vector<z *> w {&zz};

        std::cout << "template_virtualizer 1" << std::endl;
        std::cout << r1(*x[0]) << " " << r2(*x[0]) << std::endl;
        std::cout << r1(*x[1]) << " " << r2(*x[1]) << std::endl;
        std::cout << r1(*x[1]) << " " << r2(*x[1]) << std::endl;
        std::cout << r1(*w[0]) << " " << r2(*w[0]) << " " << r3(*w[0]) << std::endl;
        r4(*x[1]);
        std::cout << std::endl;
        return ok;
    }();
   
    returndata rk = [](){
        s sz = z{1};
        z zz = z{2};
        s ss = s{3};
        std::vector<std::reference_wrapper<s>> x {sz, zz, ss, *new z{9}};
        std::vector<std::reference_wrapper<z>> w {zz};

        std::cout << "template_virtualizer 2" << std::endl;
        std::cout << r1(x[0].get()) << " " << r2(x[0].get()) << std::endl;
        std::cout << r1(x[1].get()) << " " << r2(x[1].get()) << std::endl;
        std::cout << r1(x[1].get()) << " " << r2(x[1].get()) << std::endl;
        std::cout << r1(w[0].get()) << " " << r2(w[0].get()) << " " << r3(w[0].get()) << std::endl;
        r4(x[1].get());
        std::cout << std::endl;
        return ok;
    }();
}

namespace static_cat_f_to_i {
    returndata r = []() {
        std::cout << "static_cat_f_to_i" << std::endl;
        double d = 255.999 * 1;
        int ig = static_cast<int>(d);
        std::cout << ig << " " << d << std::endl;
        return ok;
    }();
}

namespace switch_goto {
    returndata r = []() {
        std::cout << "switch goto" << std::endl;
        int i = 2;
        switch(i) {
            case 1:
                std::cout << 1 << std::endl;
                return ko;
            case 2:
                std::cout << 2 << std::endl;
                goto azz;
            case 3:
                std::cout << 3 << std::endl;
                break;
            case 4: azz:
                std::cout << 4 << std::endl;
                /* fall through */
            default:
                std::cout << "default" << std::endl;
        }
        std::cout << "done" << std::endl;
       
        return ok;
    }();
}


namespace instance_to_nullptr {
    struct s {
        /** int mem[128] = {1,2,3,4}; /**/
        s * run() {
            return this;
        }
    };
   
    char buffer[1024];
    s * instance_null = static_cast<s*> (nullptr);
    s * instance_canary = static_cast<s*> ((s*) &canary);
    s * instance_heap = new s{};
    s * instance_placement_alloc_mem = new((void *) buffer) s{};
    //s * instance_placement_non_alloc_mem = new((void *) 5, 1) s{};
    //s * instance_placement_null = new((void *) nullptr) s{};
   
    returndata r = [](){
        std::cout << "instance_to_nullptr" << std::endl;
        std::cout << sizeof(s) << ' ' << sizeof(*instance_heap) << ' ' << sizeof(*instance_null) << std::endl;
        std::cout << &canary << ' ' << instance_placement_alloc_mem << ' ' /** << &(instance_heap->mem[1])  << ' ' /**/
                  << instance_heap->run() << ' ' /** << instance_canary->run()->mem[0] << ' ' */ << instance_null->run() << std::endl;
        return ok;
    }();
}

namespace magic_code_jpeg_is_valid_js_poliglot {
    // https://portswigger.net/research/bypassing-csp-using-polyglot-jpegs
    returndata r = [](){
        std::cout << "magic_code_jpeg_is_valid_js_poliglot" << std::endl;
        std::vector/*<int>*/ codes {
            // The first four bytes are a valid non-ASCII JavaScript variable 0xFF 0xD8 0xFF 0xE0
            0xFF, 0xD8, 0xFF, 0xE0,
            // Then the next two bytes specify the length of the JPEG header. If we make that length of the header 0x2F2A is multi-line JavaScript comment.
            0x2F, 0x2A,
           
            // jpeg header
            0x4A, 0x46, 0x49, 0x46, 0x00, 0x01, 0x01, 0x01, 0x00, 0x48, 0x00, 0x48,
            // 0 padding
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
           
            0xFF, 0xFE, 0x00, 0x1C,
           
            // js close comment
            0x2A, 0x2F,
           
            // alert("Burp rocks.");
            0x3D, 0x61, 0x6C, 0x65, 0x72, 0x74, 0x28, 0x22, 0x42, 0x75, 0x72, 0x70, 0x20, 0x72, 0x6F, 0x63, 0x6B, 0x73, 0x2E, 0x22, 0x29, 0x3B,
           
            // open multiline comment
            0x2F, 0x2A,
           
            // ... actual jpeg
           
            // close multiline comment
            0x2A, 0x2F,
           
            // inline comment and jpeg end image marker
            0x2F, 0x2F, 0xFF, 0xD9
        };
        std::for_each(codes.cbegin(), codes.cend(), [](auto curr){
            std::cout << static_cast<char> (curr);
        });
        std::cout << std::endl;
        return ok;
    }();
}


namespace tizio_di_twitter {
    char x[2] = {0b1001000, 0b1100100};
   
    int r = []() {
        std::cout << "tizio_di_twitter" << std::endl;
        std::cout << x << std::endl;
        return ok;
    }();
}



// int main()
// {
//     std::cout << "Hello, Wandbox!" << std::endl;
// }

// GCC reference:
//   https://gcc.gnu.org/

// C++ language references:
//   https://cppreference.com/
//   https://isocpp.org/
//   http://www.open-std.org/jtc1/sc22/wg21/

// Boost libraries references:
//   https://www.boost.org/doc/




namespace virtual_template {
struct t {};

struct t2 : t {};

struct s {
    virtual t * m(){
        std::cout << std::endl << "hello,";
        return nullptr;
    }
};

template<typename T>
struct st : s {
    T * m(){
        s::m();
        std::cout << " world!";
        return new T();
    }
};

int virtual_template = [](){
    (st<t2>{}).m();
    return 0;
}();
}

namespace const_propagation {
    struct t2 {
        int y = 42;
    };
    struct t1 {
        t2 y;
    };

    struct cont {
        t1 x;
    };
    
    
    int test = [](){
        const cont * const my = new cont;
        // my = new cont;
        // t2 new_y{};
        // new_y.y = 10;
        // my->x.y = new_y;
        std::cout << std::endl << my->x.y.y << " ciao";
        return 0;
    }();
}

int main(void){
        std::cout << std::endl << "adieu!";
}
