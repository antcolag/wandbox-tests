# wandbox tests

copiato/incollato da https://wandbox.org/

in pratica ho fatto un po' di prove brutte in c++ con wandbox (che ficata wandbox) per vedere cosa si può fare in c++

ci sono parecchi undefined behaviour e robe che funzionano solo con i settings che ho inserito (per esempio "no_if" compila solo con g++)

## settings

g++ prog.cc -Wall -Wextra -std=gnu++2b

## come funziona?

nel main non c'è praticamente niente.

È tutto nei vari namespace che contengono delle funzioni auto-chiamanti e che quindi vengono invocate prima del main
